//[Section] Create an API Server using Express

//use 'require' directive to load express module/package
//express -> this will allow us to access methods and functions that will make creating a server easier

const express = require('express')

	//create an application using express
	//express() -> creates an express/instant application
	//application is our server

const application = express();

	//identify virtual port

const port = 4000;

	//assign applpication/server into port

application.listen(port, () => console.log(`Server is running on ${port}`))